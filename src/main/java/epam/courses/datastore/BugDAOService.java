package epam.courses.datastore;

import epam.courses.model.Bug;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.util.Assert.notNull;

@Repository
public class BugDAOService {

   private static final ConcurrentHashMap<Long, Bug> BUGS = new ConcurrentHashMap<>();
   private static final AtomicLong ID_GENERATOR = new AtomicLong(0);

   public Bug create(Bug bug) {
      Long bugId = ID_GENERATOR.getAndIncrement();
      bug.setId(bugId);
      BUGS.put(bugId, bug);
      return bug;
   }

   public Bug update(Bug bug) {
      BUGS.replace(bug.getId(), bug);
      return bug;
   }

   public Bug get(Long id) {
      Bug bug = BUGS.get(id);
      notNull(bug, "The bug is not found");
      return bug;
   }
}
