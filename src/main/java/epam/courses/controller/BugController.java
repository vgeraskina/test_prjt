package epam.courses.controller;

import epam.courses.api.BugService;
import epam.courses.model.Bug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.util.Assert.isNull;
import static org.springframework.util.Assert.notNull;

@RestController
@RequestMapping("/bugs")
public class BugController {

   private final BugService bugService;

   @Autowired
   public BugController(BugService bugService) {
      this.bugService = bugService;
   }

   @PostMapping("")
   public Bug create(@RequestBody Bug bug) {
      notNull(bug, "Input parameter could not be null");
      isNull(bug.getId(), "The id must not be set");
      return bugService.create(bug);
   }

   @PutMapping("")
   public Bug update(@PathVariable("bug_id") Long id, @RequestBody Bug bug) {
      notNull(bug, "Input bug parameter could not be null");
      notNull(id, "Input id parameter could not be null");
      return bugService.update(id, bug);
   }

   @PutMapping("/assign")
   public void assignToDepartment(@PathVariable("user_id") Long userId, @PathVariable("bug_id") Long bugId) {
      notNull(bugId, "Input bugId parameter could not be null");
      bugService.assignToUser(bugId, userId);
   }
}
