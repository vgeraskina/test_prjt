package epam.courses.datastore;

import epam.courses.TestConfiguration;
import epam.courses.model.Department;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@TestConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DepartmentDAOServiceTest {
   private static final Long DEPARTMENT_ID = -8L;
   private static final Department INPUT_DEPARTMENT = new Department(DEPARTMENT_ID, "inputDepartment");
   private static DepartmentDAOService underTest;

   @BeforeClass
   public static void setUp() {
      underTest = new DepartmentDAOService();
   }

   @Test
   @Order(1)
   public void create_whenMethodCalled_thenDepartmentWithGeneratedIdIsCreated() {
      Department inputDepartment = new Department(DEPARTMENT_ID, "inputDepartment");

      Department result = underTest.create(inputDepartment);

      assertNotEquals(DEPARTMENT_ID, result.getId());
      inputDepartment.setId(result.getId());
      assertEquals(inputDepartment, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenUpdateAbsentDepartment_thenExceptionExpected() {
      underTest.update(INPUT_DEPARTMENT);
   }

   @Test
   @Order(2)
   public void update_whenUpdateExistDepartment_thenSuccess() {
      Department createdBug = createDepartment();
      Department updatedDepartment = new Department(createdBug.getId(), "updatedDepartment");

      Department result = underTest.update(updatedDepartment);

      assertEquals(updatedDepartment, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void get_whenGetAbsentDepartment_thenExceptionExpected() {
      underTest.get(DEPARTMENT_ID);
   }

   @Test
   @Order(3)
   public void get_whenGetExistDepartment_thenSuccessfullyReturned() {
      Department createdDepartment = createDepartment();

      Department result = underTest.get(createdDepartment.getId());

      assertEquals(createdDepartment, result);
   }

   @Test
   public void getAll_whenCalled_thenCollectionReturned() {
      Department createdDepartment = createDepartment();

      Collection<Department> result = underTest.getAll();

      assertTrue(result.size() > 0);
      assertTrue(result.contains(createdDepartment));
   }

   @Test
   public void getDefaultDepartment_whenCalled_thenDepartmentReturned() {
      Department result = underTest.getDefaultDepartment();

      assertNotNull(result);
   }

   private Department createDepartment() {
      Department inputDepartment = new Department(DEPARTMENT_ID, "inputDepartment");
      return underTest.create(inputDepartment);
   }
}