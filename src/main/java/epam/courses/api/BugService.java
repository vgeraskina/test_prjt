package epam.courses.api;

import epam.courses.model.Bug;

public interface BugService {

   Bug create(Bug bug);

   Bug update(Long id, Bug bug);

   Bug assignToUser(Long bugId, Long userId);
}
