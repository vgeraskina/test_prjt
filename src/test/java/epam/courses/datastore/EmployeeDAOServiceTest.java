package epam.courses.datastore;

import epam.courses.TestConfiguration;
import epam.courses.model.Employee;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@TestConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeDAOServiceTest {
   private static final Long EMPLOYEE_ID = -7L;
   private static final Long DEPARTMENT_ID = 17L;
   private static final Employee INPUT_EMPLOYEE = new Employee(EMPLOYEE_ID, "inputEmployee", DEPARTMENT_ID);
   private static EmployeeDAOService underTest;

   @BeforeClass
   public static void setUp() {
      underTest = new EmployeeDAOService();
   }

   @Test
   @Order(1)
   public void create_whenMethodCalled_thenEmployeeWithGeneratedIdIsCreated() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, "inputEmployee", DEPARTMENT_ID);

      Employee result = underTest.create(inputEmployee);

      assertNotEquals(EMPLOYEE_ID, result.getId());
      inputEmployee.setId(result.getId());
      assertEquals(inputEmployee, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenUpdateAbsentEmployee_thenExceptionExpected() {
      underTest.update(INPUT_EMPLOYEE);
   }

   @Test
   @Order(2)
   public void update_whenUpdateExistEmployee_thenSuccess() {
      Employee createdBug = createEmployee();
      Employee updatedEmployee = new Employee(createdBug.getId(), "updatedEmployee", DEPARTMENT_ID);

      Employee result = underTest.update(updatedEmployee);

      assertEquals(updatedEmployee, result);
   }


   @Test(expected = IllegalArgumentException.class)
   public void get_whenGetAbsentEmployee_thenExceptionExpected() {
      underTest.get(EMPLOYEE_ID);
   }

   @Test
   @Order(3)
   public void get_whenGetExistEmployee_thenSuccessfullyReturned() {
      Employee createdEmployee = createEmployee();

      Employee result = underTest.get(createdEmployee.getId());

      assertEquals(createdEmployee, result);
   }

   @Test
   public void getAll_whenCalled_thenCollectionReturned() {
      Employee createdEmployee = createEmployee();

      Collection<Employee> result = underTest.getAll();

      assertTrue(result.size() > 0);
      assertTrue(result.contains(createdEmployee));
   }

   private Employee createEmployee() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, "inputEmployee", DEPARTMENT_ID);
      return underTest.create(inputEmployee);
   }
}