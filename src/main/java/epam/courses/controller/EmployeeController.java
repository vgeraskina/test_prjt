package epam.courses.controller;

import epam.courses.model.Employee;
import epam.courses.api.EmployeeService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.util.Assert.isNull;
import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;
import static org.springframework.util.StringUtils.isEmpty;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
   private EmployeeService employeeService;

   public EmployeeController(EmployeeService employeeService) {
      this.employeeService = employeeService;
   }

   @PostMapping("")
   public Employee create(@RequestBody Employee employee) {
      notNull(employee, "Input parameter could not be null");
      isTrue(!isEmpty(employee.getName()), "Employee name should not be empty");
      isNull(employee.getId(), "The id must not be set");
      return employeeService.create(employee);
   }

   @PutMapping("")
   public Employee update(@PathVariable("employee_id") Long id, @RequestBody Employee employee) {
      notNull(employee, "Input employee parameter could not be null");
      notNull(id, "Input id parameter could not be null");
      return employeeService.update(id, employee);
   }
}
