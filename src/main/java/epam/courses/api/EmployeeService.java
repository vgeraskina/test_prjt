package epam.courses.api;

import epam.courses.model.Employee;

public interface EmployeeService {

   Employee create(Employee employee);

   Employee update(Long id, Employee employee);
}
