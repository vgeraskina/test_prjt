package epam.courses.service;

import epam.courses.api.BugService;
import epam.courses.datastore.BugDAOService;
import epam.courses.datastore.EmployeeDAOService;
import epam.courses.model.Bug;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static org.springframework.util.Assert.isTrue;

@Service
public class BugServiceImpl implements BugService {

   private final BugDAOService daoService;
   private final EmployeeDAOService daoEmployeeService;

   @Autowired
   public BugServiceImpl(BugDAOService daoService, EmployeeDAOService daoEmployeeService) {
      this.daoService = daoService;
      this.daoEmployeeService = daoEmployeeService;
   }

   @Override
   public Bug create(Bug bug) {
      if (bug.getUserId() != null) {
         daoEmployeeService.get(bug.getUserId());
      }
      return daoService.create(bug);
   }

   @Override
   public Bug update(Long id, Bug bug) {
      if (bug.getId() != null) {
         isTrue(id.equals(bug.getId()), "Input param inconsistency");
      }
      Bug dbBug = daoService.get(id);
      if (Objects.equals(dbBug.getUserId(), bug.getUserId()) && Objects.equals(dbBug.getDescription(), bug.getDescription())) {
         return dbBug;
      }
      if (bug.getUserId() != null) {
         daoEmployeeService.get(bug.getUserId());
      }
      merge(dbBug, bug);
      return daoService.update(bug);
   }

   @Override
   public Bug assignToUser(Long bugId, Long userId) {
      Bug dbBug = daoService.get(bugId);
      if (userId != null) {
         daoEmployeeService.get(userId);
      }
      dbBug.setUserId(userId);
      return daoService.update(dbBug);
   }

   private void merge(Bug dbBug, Bug bug) {
      dbBug.setDescription(bug.getDescription());
      dbBug.setUserId(bug.getUserId());
   }

}
