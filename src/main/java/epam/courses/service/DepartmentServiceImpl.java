package epam.courses.service;

import epam.courses.datastore.DepartmentDAOService;
import epam.courses.model.Department;
import epam.courses.model.Employee;
import epam.courses.api.DepartmentService;
import epam.courses.datastore.EmployeeDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static org.springframework.util.Assert.isTrue;

@Service
public class DepartmentServiceImpl implements DepartmentService {

   private DepartmentDAOService daoService;
   private EmployeeDAOService daoEmployeeService;

   @Autowired
   public DepartmentServiceImpl(DepartmentDAOService daoService, EmployeeDAOService daoEmployeeService) {
      this.daoService = daoService;
      this.daoEmployeeService = daoEmployeeService;
   }

   @Override
   public Department create(Department department) {
      validateDepartmentName(department);
      return daoService.create(department);
   }

   @Override
   public Department update(Long id, Department department) {
      if (department.getId() != null) {
         isTrue(id.equals(department.getId()), "Input param inconsistency");
      }
      Department dbDepartment = daoService.get(id);
      if (dbDepartment.getName().equals(department.getName())) {
         return dbDepartment;
      }
      validateDepartmentName(department);
      merge(dbDepartment, department);
      return daoService.update(dbDepartment);
   }

   @Override
   public Employee assignToDepartment(Long employeeId, Long departmentId) {
      Department dbDepartment = daoService.get(departmentId);
      Employee dbEmployee = daoEmployeeService.get(employeeId);
      dbEmployee.setDepartmentId(dbDepartment.getId());
      return daoEmployeeService.update(dbEmployee);
   }

   @Override
   public Collection<Employee> getAllEmployees() {
      return daoEmployeeService.getAll();
   }

   private void merge(Department dbDepartment, Department department) {
      dbDepartment.setName(department.getName());
   }

   private void validateDepartmentName(Department department) {
      isTrue(daoService.getAll().stream().noneMatch(dbEntry -> dbEntry.getName().equals(department.getName())),
              "The department with the name " + department.getName() + " already exists");
   }

}
