package epam.courses.controller;

import epam.courses.TestConfiguration;
import epam.courses.api.BugService;
import epam.courses.model.Bug;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class BugControllerTest {

   private static final Long BUG_ID = 12L;
   private static final Long USER_ID = 11L;
   private static final Bug DB_BUG = new Bug(BUG_ID, "dataBaseBug", USER_ID);
   private static final Bug INPUT_BUG = new Bug(null, "inputBug", null);

   @Mock
   private BugService bugService;
   @InjectMocks
   private BugController underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(bugService.create(any(Bug.class))).thenReturn(DB_BUG);
      when(bugService.update(any(Long.class), any(Bug.class))).thenReturn(DB_BUG);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenNullPassed_thenExceptionExpected() {
      underTest.create(null);

      verifyZeroInteractions(bugService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenIdIsPredefined_thenExceptionExpected() {
      underTest.create(new Bug(1L, null, null));

      verifyZeroInteractions(bugService);
   }

   @Test
   public void create_whenCorrectParamPassed_thenServiceToCreateCalled() {
      Bug result = underTest.create(INPUT_BUG);

      verify(bugService).create(INPUT_BUG);
      assertSame(DB_BUG, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenIdParamNull_thenExceptionExpected() {
      underTest.update(null, INPUT_BUG);

      verifyZeroInteractions(bugService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenBugParamNull_thenExceptionExpected() {
      underTest.update(BUG_ID, null);

      verifyZeroInteractions(bugService);
   }

   @Test
   public void update_whenCorrectParamPassed_thenServiceToUpdateCalled() {
      Bug result = underTest.update(BUG_ID, INPUT_BUG);

      verify(bugService).update(BUG_ID, INPUT_BUG);
      assertSame(DB_BUG, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void assignToDepartment_whenBugIdParamNull_thenExceptionExpected() {
      underTest.assignToDepartment(USER_ID, null);

      verifyZeroInteractions(bugService);
   }

   @Test
   public void assignToDepartment_whenUserParamNull_thenServiceToAssignCalled() {
      underTest.assignToDepartment(null, BUG_ID);

      verify(bugService).assignToUser(BUG_ID, null);
   }

   @Test
   public void assignToDepartment_whenAllParamPassed_thenServiceToAssignCalled() {
      underTest.assignToDepartment(USER_ID, BUG_ID);

      verify(bugService).assignToUser(BUG_ID, USER_ID);
   }
}