package epam.courses.controller;

import epam.courses.TestConfiguration;
import epam.courses.api.EmployeeService;
import epam.courses.model.Employee;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class EmployeeControllerTest {
   
   private static final Long EMPLOYEE_ID = 12L;
   private static final Long DEPARTMENT_ID = 11L;
   private static final Employee DB_EMPLOYEE = new Employee(EMPLOYEE_ID, "dataBaseEmployee", DEPARTMENT_ID);
   private static final Employee INPUT_EMPLOYEE = new Employee(null, "inputEmployee", null);
   
   @Mock
   private EmployeeService employeeService;
   @InjectMocks
   private EmployeeController underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(employeeService.create(any(Employee.class))).thenReturn(DB_EMPLOYEE);
      when(employeeService.update(any(Long.class), any(Employee.class))).thenReturn(DB_EMPLOYEE);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenNullPassed_thenExceptionExpected() {
      underTest.create(null);

      verifyZeroInteractions(employeeService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenIdIsPredefined_thenExceptionExpected() {
      underTest.create(new Employee(1L, null, null));

      verifyZeroInteractions(employeeService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenEmployeeNameEmpty_thenExceptionExpected() {
      underTest.create(new Employee(null, "", DEPARTMENT_ID));

      verifyZeroInteractions(employeeService);
   }

   @Test
   public void create_whenCorrectParamPassed_thenServiceToCreateCalled() {
      Employee result = underTest.create(INPUT_EMPLOYEE);

      verify(employeeService).create(INPUT_EMPLOYEE);
      assertSame(DB_EMPLOYEE, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenIdParamNull_thenExceptionExpected() {
      underTest.update(null, INPUT_EMPLOYEE);

      verifyZeroInteractions(employeeService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenEmployeeParamNull_thenExceptionExpected() {
      underTest.update(EMPLOYEE_ID, null);

      verifyZeroInteractions(employeeService);
   }

   @Test
   public void update_whenCorrectParamPassed_thenServiceToUpdateCalled() {
      Employee result = underTest.update(EMPLOYEE_ID, INPUT_EMPLOYEE);

      verify(employeeService).update(EMPLOYEE_ID, INPUT_EMPLOYEE);
      assertSame(DB_EMPLOYEE, result);
   }
}