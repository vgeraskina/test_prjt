package epam.courses.api;

import epam.courses.model.Department;
import epam.courses.model.Employee;

import java.util.Collection;

public interface DepartmentService {

   Department create(Department department);

   Department update(Long id, Department department);

   Employee assignToDepartment(Long employeeId, Long departmentId);

   Collection<Employee> getAllEmployees();
}
