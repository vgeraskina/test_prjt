package epam.courses.controller;

import epam.courses.model.Employee;
import epam.courses.api.DepartmentService;
import epam.courses.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.util.Assert.isNull;
import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;
import static org.springframework.util.StringUtils.isEmpty;

@RestController
@RequestMapping("/department")
public class DepartmentController {

   private DepartmentService departmentService;

   @Autowired
   public DepartmentController(DepartmentService departmentService) {
      this.departmentService = departmentService;
   }

   @PostMapping("")
   public Department create(@RequestBody Department department) {
      notNull(department, "Input parameter could not be null");
      isNull(department.getId(), "The id must not be set");
      isTrue(!isEmpty(department.getName()), "Department name should not be empty");
      return departmentService.create(department);
   }

   @PutMapping("")
   public Department update(@PathVariable("department_id") Long id, @RequestBody Department department) {
      notNull(department, "Input department parameter could not be null");
      notNull(id, "Input id parameter could not be null");
      isTrue(!isEmpty(department.getName()), "Department name should not be empty");
      return departmentService.update(id, department);
   }

   @PutMapping("/assign")
   public Employee assignToDepartment(@PathVariable("employee_id") Long employeeId, @PathVariable("department_id") Long departmentId) {
      notNull(employeeId, "Input employeeId parameter could not be null");
      notNull(departmentId, "Input departmentId parameter could not be null");
      return departmentService.assignToDepartment(employeeId, departmentId);
   }

   @GetMapping("")
   public Collection<Employee> getAllEmployees() {
      return departmentService.getAllEmployees();
   }

}
