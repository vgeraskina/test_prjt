package epam.courses.service;

import epam.courses.TestConfiguration;
import epam.courses.datastore.BugDAOService;
import epam.courses.datastore.EmployeeDAOService;
import epam.courses.model.Bug;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class BugServiceImplTest {

   private static final Long BUG_ID = 12L;
   private static final Long USER_ID = 11L;
   private static final Bug ASSIGNED_BUG = new Bug(BUG_ID, "assignedBug", USER_ID);

   @Mock
   private BugDAOService daoService;
   @Mock
   private EmployeeDAOService daoEmployeeService;
   @InjectMocks
   private BugServiceImpl underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(daoService.create(any(Bug.class))).thenReturn(ASSIGNED_BUG);
   }

   @Test
   public void create_whenCreateWithPreAssign_thenVerifyUserExistence() {
      Bug inputBug = new Bug(BUG_ID, "inputBug", USER_ID);

      Bug result = underTest.create(inputBug);

      verify(daoEmployeeService).get(USER_ID);
      verify(daoService).create(inputBug);
      assertSame(ASSIGNED_BUG, result);
   }

   @Test
   public void create_whenCreateWithoutAssign_thenSimpleCreation() {
      Bug inputBug = new Bug(BUG_ID, "inputBug", null);

      Bug result = underTest.create(inputBug);

      verify(daoService).create(inputBug);
      assertSame(ASSIGNED_BUG, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenInputParamsInconsistent_thenExceptionExpected() {
      Bug inputBug = new Bug(BUG_ID, "inputBug", null);

      underTest.update(BUG_ID + 1, inputBug);

      verifyZeroInteractions(daoService);
   }

   @Test
   public void update_whenNoUpdates_thenSimpleReturn() {
      when(daoService.get(BUG_ID)).thenReturn(ASSIGNED_BUG);

      Bug result = underTest.update(BUG_ID, ASSIGNED_BUG);

      assertSame(ASSIGNED_BUG, result);
      verify(daoService, never()).update(any(Bug.class));
   }

   @Test
   public void update_whenUserAssign_thenValidateUserExistence() {
      Bug dbBug = new Bug(BUG_ID, "inputBug", null);
      when(daoService.get(BUG_ID)).thenReturn(dbBug);
      when(daoService.update(dbBug)).thenReturn(dbBug);

      Bug result = underTest.update(BUG_ID, ASSIGNED_BUG);

      verify(daoEmployeeService).get(USER_ID);
      verify(daoService).update(dbBug);
      assertEquals(USER_ID, result.getUserId());
   }

   @Test
   public void update_whenDescriptionEdited_thenUpdateDescription() {
      String newDescription = "newDescription";
      Bug dbBug = new Bug(BUG_ID, "inputBug", null);
      Bug inputBug = new Bug(BUG_ID, newDescription, null);
      when(daoService.get(BUG_ID)).thenReturn(dbBug);
      when(daoService.update(dbBug)).thenReturn(dbBug);

      Bug result = underTest.update(BUG_ID, inputBug);

      verify(daoService).update(dbBug);
      assertEquals(newDescription, result.getDescription());
   }

   @Test
   public void assignToUser_whenAssignToUser_thenVerifyUserExistence() {
      Bug dbBug = new Bug(BUG_ID, "inputBug", null);
      when(daoService.get(BUG_ID)).thenReturn(dbBug);
      when(daoService.update(dbBug)).thenReturn(dbBug);

      Bug result = underTest.assignToUser(BUG_ID, USER_ID);

      verify(daoEmployeeService).get(USER_ID);
      verify(daoService).update(dbBug);
      assertEquals(USER_ID, result.getUserId());
   }

   @Test
   public void assignToUser_whenUnAssign_thenSimpleUpdate() {
      Bug dbBug = new Bug(BUG_ID, "inputBug", USER_ID);
      when(daoService.get(BUG_ID)).thenReturn(dbBug);
      when(daoService.update(dbBug)).thenReturn(dbBug);

      Bug result = underTest.assignToUser(BUG_ID, null);

      verify(daoService).update(dbBug);
      assertNull(result.getUserId());
   }
}