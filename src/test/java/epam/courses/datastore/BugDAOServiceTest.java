package epam.courses.datastore;

import epam.courses.TestConfiguration;
import epam.courses.model.Bug;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@TestConfiguration
@TestMethodOrder(OrderAnnotation.class)
public class BugDAOServiceTest {

   private static final Long BUG_ID = -9L;
   private static final Bug INPUT_BUG = new Bug(BUG_ID, "inputBug", null);
   private static BugDAOService underTest;

   @BeforeClass
   public static void setUp() {
      underTest = new BugDAOService();
   }

   @Test
   @Order(1)
   public void create_whenMethodCalled_thenBugWithGeneratedIdIsCreated() {
      Bug inputBug = new Bug(BUG_ID, "inputBug", null);

      Bug result = underTest.create(inputBug);

      assertNotEquals(BUG_ID, result.getId());
      inputBug.setId(result.getId());
      assertEquals(inputBug, result);
   }

   @Test
   public void update_whenUpdateAbsentBug_thenExceptionExpected() {
      Bug result = underTest.update(INPUT_BUG);

      assertEquals(INPUT_BUG, result);
   }

   @Test
   @Order(2)
   public void update_whenUpdateExistBug_thenSuccess() {
      Bug createdBug = createBug();
      Bug updatedBug = new Bug(createdBug.getId(), "updatedBug", 12L);

      Bug result = underTest.update(updatedBug);

      assertEquals(updatedBug, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void get_whenGetAbsentBug_thenExceptionExpected() {
      underTest.get(BUG_ID);
   }

   @Test
   @Order(3)
   public void get_whenGetExistBug_thenSuccessfullyReturned() {
      Bug createdBug = createBug();

      Bug result = underTest.get(createdBug.getId());

      assertEquals(createdBug, result);
   }

   private Bug createBug() {
      Bug inputBug = new Bug(BUG_ID, "inputBug", null);
      return underTest.create(inputBug);
   }
}