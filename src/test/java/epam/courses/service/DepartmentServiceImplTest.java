package epam.courses.service;

import epam.courses.TestConfiguration;
import epam.courses.datastore.DepartmentDAOService;
import epam.courses.datastore.EmployeeDAOService;
import epam.courses.model.Department;
import epam.courses.model.Employee;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class DepartmentServiceImplTest {

   private static final Long DEPARTMENT_ID = 13L;
   private static final String DB_DEPARTMENT_NAME = "dbDepartment";
   private static final String ANOTHER_DEPARTMENT_NAME = "anotherDepartment";
   private static final Long USER_ID = 12L;
   private static final Department DB_DEPARTMENT = new Department(DEPARTMENT_ID + 1, DB_DEPARTMENT_NAME);

   @Mock
   private DepartmentDAOService daoService;
   @Mock
   private EmployeeDAOService daoEmployeeService;
   @InjectMocks
   private DepartmentServiceImpl underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(daoService.getAll()).thenReturn(singletonList(DB_DEPARTMENT));
      when(daoService.create(any(Department.class))).thenReturn(DB_DEPARTMENT);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenCreateWithExistName_thenExceptionExpected() {
      Department inputDepartment = new Department(DEPARTMENT_ID + 1, DB_DEPARTMENT_NAME);

      underTest.create(inputDepartment);

      verifyZeroInteractions(daoService);
      verify(daoService).getAll();
   }

   @Test
   public void create_whenCreateWithNewName_thenSimpleCreation() {
      Department inputDepartment = new Department(DEPARTMENT_ID, ANOTHER_DEPARTMENT_NAME);

      Department result = underTest.create(inputDepartment);

      verify(daoService).create(inputDepartment);
      assertSame(DB_DEPARTMENT, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenInputParamsInconsistent_thenExceptionExpected() {
      Department inputDepartment = new Department(DEPARTMENT_ID, "inputDepartment");

      underTest.update(DEPARTMENT_ID + 1, inputDepartment);

      verifyZeroInteractions(daoService);
   }

   @Test
   public void update_whenNoUpdates_thenSimpleReturn() {
      when(daoService.get(DEPARTMENT_ID + 1)).thenReturn(DB_DEPARTMENT);

      Department result = underTest.update(DEPARTMENT_ID + 1, DB_DEPARTMENT);

      assertSame(DB_DEPARTMENT, result);
      verify(daoService, never()).update(any(Department.class));
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenUpdateWithExistName_thenExceptionExpected() {
      Department inputDepartment = new Department(DEPARTMENT_ID, DB_DEPARTMENT_NAME);
      Department dbDepartment = new Department(DEPARTMENT_ID, ANOTHER_DEPARTMENT_NAME);
      when(daoService.get(DEPARTMENT_ID)).thenReturn(dbDepartment);

      underTest.update(DEPARTMENT_ID, inputDepartment);

      verifyZeroInteractions(daoService);
      verify(daoService).getAll();
   }

   @Test
   public void update_whenNameCorrectlyEdited_thenUpdateDepartment() {
      Department inputDepartment = new Department(DEPARTMENT_ID, ANOTHER_DEPARTMENT_NAME);
      Department dbDepartment = new Department(DEPARTMENT_ID, DB_DEPARTMENT_NAME);
      when(daoService.get(DEPARTMENT_ID)).thenReturn(dbDepartment);
      when(daoService.update(dbDepartment)).thenReturn(dbDepartment);

      Department result = underTest.update(DEPARTMENT_ID, inputDepartment);

      verify(daoService).update(dbDepartment);
      assertEquals(ANOTHER_DEPARTMENT_NAME, result.getName());
   }

   @Test
   public void assignToDepartment_whenCalled_thenAssignPerformedForUser() {
      Employee foundEmployee = new Employee(USER_ID, "foundUser", null);
      when(daoService.get(DEPARTMENT_ID)).thenReturn(DB_DEPARTMENT);
      when(daoEmployeeService.get(USER_ID)).thenReturn(foundEmployee);
      when(daoEmployeeService.update(foundEmployee)).thenReturn(foundEmployee);

      Employee result = underTest.assignToDepartment(USER_ID, DEPARTMENT_ID);

      assertEquals(DB_DEPARTMENT.getId(), result.getDepartmentId());
   }

   @Test
   public void getAllEmployees_whenCalled_thenCollectionReturned() {
      List<Employee> expectedResult = singletonList(new Employee(USER_ID, "name", DEPARTMENT_ID));
      when(daoEmployeeService.getAll()).thenReturn(expectedResult);

      Collection<Employee> result = underTest.getAllEmployees();

      assertSame(expectedResult, result);
   }
}