package epam.courses.controller;

import epam.courses.TestConfiguration;
import epam.courses.api.DepartmentService;
import epam.courses.model.Department;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class DepartmentControllerTest {

   private static final Long USER_ID = 12L;
   private static final Long DEPARTMENT_ID = 13L;
   private static final Department DB_DEPARTMENT = new Department(DEPARTMENT_ID, "dataBaseDepartment");
   private static final Department INPUT_DEPARTMENT = new Department(null, "inputDepartment");

   @Mock
   private DepartmentService departmentService;
   @InjectMocks
   private DepartmentController underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(departmentService.create(any(Department.class))).thenReturn(DB_DEPARTMENT);
      when(departmentService.update(any(Long.class), any(Department.class))).thenReturn(DB_DEPARTMENT);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenNullPassed_thenExceptionExpected() {
      underTest.create(null);

      verifyZeroInteractions(departmentService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenIdIsPredefined_thenExceptionExpected() {
      underTest.create(new Department(1L, null));

      verifyZeroInteractions(departmentService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void create_whenDepartmentNameEmpty_thenExceptionExpected() {
      underTest.create(new Department(null, ""));

      verifyZeroInteractions(departmentService);
   }

   @Test
   public void create_whenCorrectParamPassed_thenServiceToCreateCalled() {
      Department result = underTest.create(INPUT_DEPARTMENT);

      verify(departmentService).create(INPUT_DEPARTMENT);
      assertSame(DB_DEPARTMENT, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenIdParamNull_thenExceptionExpected() {
      underTest.update(null, INPUT_DEPARTMENT);

      verifyZeroInteractions(departmentService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenDepartmentParamNull_thenExceptionExpected() {
      underTest.update(DEPARTMENT_ID, null);

      verifyZeroInteractions(departmentService);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenDepartmentNameEmpty_thenExceptionExpected() {
      underTest.update(DEPARTMENT_ID, new Department(null, ""));

      verifyZeroInteractions(departmentService);
   }

   @Test
   public void update_whenCorrectParamPassed_thenServiceToUpdateCalled() {
      Department result = underTest.update(DEPARTMENT_ID, INPUT_DEPARTMENT);

      verify(departmentService).update(DEPARTMENT_ID, INPUT_DEPARTMENT);
      assertSame(DB_DEPARTMENT, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void assignToDepartment_whenDepartmentIdParamNull_thenExceptionExpected() {
      underTest.assignToDepartment(USER_ID, null);

      verifyZeroInteractions(departmentService);
   }

   @Test
   public void assignToDepartment_whenAllParamPassed_thenServiceToAssignCalled() {
      underTest.assignToDepartment(USER_ID, DEPARTMENT_ID);

      verify(departmentService).assignToDepartment(USER_ID, DEPARTMENT_ID);
   }

   @Test
   public void getAllEmployees_whenCalled_thenAppropriateServiceCalled() {
      underTest.getAllEmployees();

      verify(departmentService).getAllEmployees();
   }
}