package epam.courses.model;

import java.util.Objects;

public class Bug {
   private Long id;
   private String description;
   private Long userId;

   public Bug() {
   }

   public Bug(Long id, String description, Long userId) {
      this.id = id;
      this.description = description;
      this.userId = userId;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof Bug)) {
         return false;
      }
      Bug bug = (Bug) o;
      return Objects.equals(id, bug.id) && Objects.equals(description, bug.description) && Objects.equals(userId, bug.userId);
   }

   @Override
   public int hashCode() {
      return Objects.hash(id, description, userId);
   }
}
