package epam.courses.datastore;

import epam.courses.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

@Repository
public class EmployeeDAOService {

   private static final ConcurrentHashMap<Long, Employee> EMPLOYEES = new ConcurrentHashMap<Long, Employee>() {{
      put(-1L, new Employee(-1L, "Ghost", -1L));
   }};
   private static final AtomicLong ID_GENERATOR = new AtomicLong(0);

   public Employee create(Employee employee) {
      Long employeeId = ID_GENERATOR.getAndIncrement();
      employee.setId(employeeId);
      EMPLOYEES.put(employeeId, employee);
      return employee;
   }

   public Employee update(Employee employee) {
      isTrue(EMPLOYEES.containsKey(employee.getId()), "The employee is not found");
      EMPLOYEES.put(employee.getId(), employee);
      return employee;
   }

   public Employee get(Long employeeId) {
      Employee employee = EMPLOYEES.get(employeeId);
      notNull(employee, "The employee is not found");
      return employee;
   }

   public Collection<Employee> getAll() {
      return EMPLOYEES.values();
   }
}
