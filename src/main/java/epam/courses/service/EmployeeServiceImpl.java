package epam.courses.service;

import epam.courses.api.EmployeeService;
import epam.courses.datastore.DepartmentDAOService;
import epam.courses.model.Employee;
import epam.courses.datastore.EmployeeDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import static org.springframework.util.Assert.isTrue;

@Service
public class EmployeeServiceImpl implements EmployeeService {

   private EmployeeDAOService daoService;
   private DepartmentDAOService daoDepartmentService;

   @Autowired
   public EmployeeServiceImpl(EmployeeDAOService daoService, DepartmentDAOService daoDepartmentService) {
      this.daoService = daoService;
      this.daoDepartmentService = daoDepartmentService;
   }

   @Override
   public Employee create(Employee employee) {
      if (employee.getDepartmentId() == null) {
         employee.setDepartmentId(daoDepartmentService.getDefaultDepartment().getId());
      } else {
         daoDepartmentService.get(employee.getDepartmentId());
      }
      return daoService.create(employee);
   }

   @Override
   public Employee update(Long id, Employee employee) {
      if (employee.getId() != null) {
         isTrue(id.equals(employee.getId()), "Input param inconsistency");
      }
      Employee dbEmployee = daoService.get(id);
      boolean sameName = StringUtils.isEmpty(employee.getName()) || dbEmployee.getName().equals(employee.getName());
      if (sameName && dbEmployee.getDepartmentId().equals(employee.getDepartmentId())) {
         return dbEmployee;
      }
      if (!sameName) {
         validateEmployeeName(employee);
      }
      if (employee.getDepartmentId() != null) {
         daoDepartmentService.get(employee.getDepartmentId());
      }
      merge(dbEmployee, employee);
      return daoService.update(dbEmployee);
   }

   private void merge(Employee dbEmployee, Employee employee) {
      if (!StringUtils.isEmpty(employee.getName())) {
         dbEmployee.setName(employee.getName());
      }
      if (employee.getDepartmentId() != null) {
         dbEmployee.setDepartmentId(employee.getDepartmentId());
      }
   }

   private void validateEmployeeName(Employee employee) {
      isTrue(daoService.getAll().stream().noneMatch(dbEntry -> dbEntry.getName().equals(employee.getName())),
              "The employee with the name " + employee.getName() + " already exists");
   }

}
