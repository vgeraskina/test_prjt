package epam.courses.service;

import epam.courses.TestConfiguration;
import epam.courses.datastore.DepartmentDAOService;
import epam.courses.datastore.EmployeeDAOService;
import epam.courses.model.Department;
import epam.courses.model.Employee;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@TestConfiguration
public class EmployeeServiceImplTest {
   private static final Long EMPLOYEE_ID = 14L;
   private static final Long DEFAULT_DEPARTMENT_ID = 444L;
   private static final Long DEPARTMENT_ID = 44L;
   private static final Employee DB_EMPLOYEE = new Employee(EMPLOYEE_ID, "dbEmployee", DEFAULT_DEPARTMENT_ID);

   @Mock
   private EmployeeDAOService daoService;
   @Mock
   private DepartmentDAOService daoDepartmentService;
   @InjectMocks
   private EmployeeServiceImpl underTest;

   @Before
   public void setUp() {
      initMocks(this);
      when(daoDepartmentService.getDefaultDepartment()).thenReturn(new Department(DEFAULT_DEPARTMENT_ID, null));
      when(daoService.create(any(Employee.class))).thenReturn(DB_EMPLOYEE);
   }

   @Test
   public void create_whenNoDepartment_thenDefaultDepartmentSet() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, "John Watson", null);

      Employee result = underTest.create(inputEmployee);

      assertEquals(DEFAULT_DEPARTMENT_ID, inputEmployee.getDepartmentId());
      assertSame(DB_EMPLOYEE, result);
   }

   @Test
   public void create_whenDepartmentSet_thenValidateDepartmentExistence() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, "John Watson", DEPARTMENT_ID);

      Employee result = underTest.create(inputEmployee);

      assertEquals(DEPARTMENT_ID, inputEmployee.getDepartmentId());
      verify(daoDepartmentService).get(DEPARTMENT_ID);
      assertSame(DB_EMPLOYEE, result);
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenInputParamsInconsistent_thenExceptionExpected() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, "inputEmployee", DEPARTMENT_ID);

      underTest.update(EMPLOYEE_ID + 1, inputEmployee);

      verifyZeroInteractions(daoService);
   }

   @Test
   public void update_whenNoUpdates_thenSimpleReturn() {
      when(daoService.get(EMPLOYEE_ID)).thenReturn(DB_EMPLOYEE);

      Employee result = underTest.update(EMPLOYEE_ID, DB_EMPLOYEE);

      assertSame(DB_EMPLOYEE, result);
      verify(daoService, never()).update(any(Employee.class));
   }

   @Test
   public void update_whenSameDepartmentNullName_thenSimpleReturn() {
      Employee inputEmployee = new Employee(EMPLOYEE_ID, null, DEFAULT_DEPARTMENT_ID);
      when(daoService.get(EMPLOYEE_ID)).thenReturn(DB_EMPLOYEE);

      Employee result = underTest.update(EMPLOYEE_ID, inputEmployee);

      assertSame(DB_EMPLOYEE, result);
      verify(daoService, never()).update(any(Employee.class));
   }

   @Test(expected = IllegalArgumentException.class)
   public void update_whenUpdateWithExistName_thenExceptionExpected() {
      String existName = "old employee";
      Employee inputEmployee = new Employee(EMPLOYEE_ID, existName, DEPARTMENT_ID);
      when(daoService.get(EMPLOYEE_ID)).thenReturn(DB_EMPLOYEE);
      when(daoService.getAll()).thenReturn(asList(DB_EMPLOYEE, new Employee(null, existName, null)));

      underTest.update(EMPLOYEE_ID, inputEmployee);

      verifyZeroInteractions(daoService);
      verify(daoService).getAll();
   }

   @Test
   public void update_whenDepartmentUpdated_thenSuccessAfterDepartmentExistenceValidation () {
      Long newDepartmentId = DEPARTMENT_ID + 1;
      String name = "existName";
      Employee inputEmployee = new Employee(null, name, newDepartmentId);
      Employee dbEmployee = new Employee(EMPLOYEE_ID, name, DEPARTMENT_ID);
      when(daoService.get(EMPLOYEE_ID)).thenReturn(dbEmployee);
      when(daoService.update(any(Employee.class))).thenReturn(dbEmployee);

      Employee result = underTest.update(EMPLOYEE_ID, inputEmployee);

      verify(daoDepartmentService).get(newDepartmentId);
      verify(daoService).update(dbEmployee);
      assertEquals(newDepartmentId, result.getDepartmentId());
   }

   @Test
   public void update_whenNullNameDepartmentUpdated_thenSuccessAfterDepartmentExistenceValidation () {
      Long newDepartmentId = DEPARTMENT_ID + 1;
      String name = "existName";
      Employee inputEmployee = new Employee(null, null, newDepartmentId);
      Employee dbEmployee = new Employee(EMPLOYEE_ID, name, DEPARTMENT_ID);
      when(daoService.get(EMPLOYEE_ID)).thenReturn(dbEmployee);
      when(daoService.update(any(Employee.class))).thenReturn(dbEmployee);

      Employee result = underTest.update(EMPLOYEE_ID, inputEmployee);

      verify(daoDepartmentService).get(newDepartmentId);
      verify(daoService).update(dbEmployee);
      assertEquals(newDepartmentId, result.getDepartmentId());
      assertEquals(name, result.getName());
   }

   @Test
   public void update_whenNameUpdated_thenSuccess () {
      String name = "newName";
      Employee inputEmployee = new Employee(null, name, null);
      Employee dbEmployee = new Employee(EMPLOYEE_ID, "oldName", DEPARTMENT_ID);
      when(daoService.get(EMPLOYEE_ID)).thenReturn(dbEmployee);
      when(daoService.update(any(Employee.class))).thenReturn(dbEmployee);

      Employee result = underTest.update(EMPLOYEE_ID, inputEmployee);

      assertEquals(DEPARTMENT_ID, result.getDepartmentId());
      verify(daoService).update(dbEmployee);
      assertEquals(name, result.getName());
   }
}