package epam.courses.datastore;

import epam.courses.model.Department;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.util.Assert.isTrue;
import static org.springframework.util.Assert.notNull;

@Repository
public class DepartmentDAOService {

   private static final Department ROOT_DEPARTMENT = new Department(-1L, "ROOT");
   private static final ConcurrentHashMap<Long, Department> DEPARTMENTS = new ConcurrentHashMap<Long, Department>() {{
      this.put(-1L, ROOT_DEPARTMENT);
   }};
   private static final AtomicLong ID_GENERATOR = new AtomicLong(0);

   public Department create(Department department) {
      Long departmentId = ID_GENERATOR.getAndIncrement();
      department.setId(departmentId);
      DEPARTMENTS.put(departmentId, department);
      return department;
   }

   public Department update(Department department) {
      isTrue(DEPARTMENTS.containsKey(department.getId()), "The department is not found");
      DEPARTMENTS.replace(department.getId(), department);
      return department;
   }

   public Department get(Long departmentId) {
      Department department = DEPARTMENTS.get(departmentId);
      notNull(department, "The department is not found");
      return department;
   }

   public Collection<Department> getAll() {
      return DEPARTMENTS.values();
   }

   public Department getDefaultDepartment() {
      return ROOT_DEPARTMENT;
   }
}
